<?php // $Id$

function agencydark_preprocess_page(&$vars) {
  $vars['ie6'] = FALSE;
  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0') !== false)){ $vars['ie6'] = TRUE; }
  $vars['footer_message'] = $vars['footer_message'] . "<div id=\"credits\"><a title=\"Realizzazione template Drupal\" href=\"http://www.themes-drupal.org\">T</a><a title=\"Sviluppo siti Drupal\" href=\"http://www.siti-drupal.it\">D</a></div>";
}
